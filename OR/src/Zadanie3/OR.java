/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Zadanie3;

/**
 * aplikacja implementujaca problem producenta i konsumenta, rozwiazanie z
 * uzyciem monitora
 */
import java.util.concurrent.locks.*;
import java.util.*;
import java.util.Random;

public class OR {

    /**
     * zasob
     */
    static Zasob zasob;

    public static void main(String[] args) {
        // utworz zasob
        zasob = new Zasob(11);
        // utworz wątki  i konsumenta
        // utworzenie watkow
        new B(zasob);

        A a[] = new A[4];
        for (int i = 0; i < 4; i++) {
            a[i] = new A(zasob, i);
        }

    }
}

/**
 * klasa producenta
 */
class A implements Runnable {

    /**
     * zasob
     */
    Zasob zasob;
    int id;

    /**
     * konstruktor
     *
     * @param zasob referencja do zasobu
     */
    A(Zasob zasob, int id) {
        this.zasob = zasob;
        this.id = id;

        // uruchom watek producenta
        new Thread(this).start();
    }

    /**
     * metoda run watku producenta
     */
    public void run() {
        // utworzenie generatora liczb pseudolosowych
        Random rnd = new Random();
        rnd.setSeed((new GregorianCalendar()).getTimeInMillis());

        // umieszczanie liczb w zasobie
        int[] number = new int[11];
        for (int j = 0; j < 20; ++j) {
            try {
                Thread.sleep(50);		// uspij producenta na 500 milisekund
            }
            catch (InterruptedException e) {
            }
            for (int i = 1; i < 11; ++i) {
                //number[i] = Math.abs(rnd.nextInt() % 200);
                number[i] = this.id;
            }
            //System.out.println("Liczba " + number + " zostala wyprodukowana");

            zasob.put(number, this);

            //System.out.println("Liczba " + number + " zostala wstawiona do zasobu");
        }

        System.out.println("A zakonczyl prace");
    }
}

/**
 * klasa konsumenta
 */
class B implements Runnable {

    /**
     * zasob
     */
    Zasob zasob;
    int[] suma = new int[4];
    int[] paczki = new int[4];

    /**
     * konstruktor
     *
     * @param zasob referencja do zasobu
     */
    B(Zasob zasob) {
        this.zasob = zasob;

        // uruchom watek konsumenta
        new Thread(this).start();
    }

    /**
     * metoda run watku konsumenta
     */
    public void run() {
        int[] number;
        boolean praca = true;

        while (praca) {
            try {
                Thread.sleep(10);		// uspij konsumenta na 500 milisekund
            }
            catch (InterruptedException e) {
            }

            // pobierz liczbe z zasobu
            number = zasob.get(zasob.buffer[0]);
            int iden = zasob.buffer[0];
            for (int i = 1; i < 11; ++i) {

                suma[iden] += number[i];
                
            }
            paczki[iden]++;

            if (paczki[iden] == 20) {
                    System.out.println("Suma paczki " + iden + " = " + suma[iden]);
                }
            if ((paczki[0] == 20) && (paczki[1] == 20) && (paczki[2] == 20) && (paczki[3] == 20)) {
                praca = false;
            }
            //  System.out.println("Liczba " + number + " zostala skonsumowana");
        }
        System.out.println("B zakonczyl prace");
    }
}

/**
 * klasa zasobu
 */
class Zasob {

    /**
     * rozmiar bufora
     */
    private final int N;

    /**
     * bufor przechowujacy liczby
     */
    int buffer[];

    /**
     * indeksy okreslajace wejscie i wyjscie bufora oraz liczbe zajetych miejsc
     */
    int input, output, fill;

    //tzw. monitor
    final Lock protection = new ReentrantLock();
    final Condition ConB = protection.newCondition(); //kolejki zatrzymanych watkow - powiazany z obiektem typu lock
    final List<Condition> ConA = new ArrayList<Condition>();

    void createCon() {
        for (int i = 0; i < 4; ++i) {
            ConA.add(protection.newCondition());
        }
    }

    /**
     * konstruktor
     *
     * @param n rozmiar bufora
     */
    public Zasob(int n) {
        N = n;
        createCon();
        // utworzenie bufora
        buffer = new int[N];

        input = output = fill = 0;
    }

    /**
     * umieszczanie liczby w zasobie (buforze)
     *
     * @param number liczba umieszczana w zasobie
     */
    void put(int[] number, A a) {
        protection.lock(); // nie ma wspolpierznosci

        try {
            // oczekiwanie na wolne miejsce w buforze
            while (fill == 1) {
                ConA.get(a.id).await();
            }
            buffer[0] = a.id; 
            for (int i = 0; i < 10; ++i) {
                buffer[i+1] = number[i];
            }
            
            fill = 1;
            ConB.signal();

        }
        catch (InterruptedException e) {
        }

        protection.unlock();
    }

    /**
     * pobranie liczby z zasobu
     *
     * @return liczba przechowywana w zasobie
     */
    int[] get(int id) {
        protection.lock();

        int[] number = new int[11];

        try {
            // oczekiwanie na wolne miejsce w buforze
            while (fill == 0) {
                ConB.await();
            }
            for (int i = 0; i < number.length; ++i) {
                number[i] = buffer[i];

            }
            fill = 0;
            for (int i = 3; i >= 0; i--) {
                ConA.get(i).signal();
            }

        }
        catch (InterruptedException e) {
        }

        protection.unlock();

        return number;
    }
}
